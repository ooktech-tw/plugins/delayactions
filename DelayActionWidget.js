/*\
title: $:/plugins/OokTech/DelayActions/action-delay.js
type: application/javascript
module-type: widget

Action widget that waits a given amount of time then executes other action
widgets.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

const Widget = require("$:/core/modules/widgets/widget.js").widget;

const DelayAction = function(parseTreeNode,options) {
  this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
DelayAction.prototype = new Widget();

/*
Render this widget into the DOM
*/
DelayAction.prototype.render = function(parent,nextSibling) {
  this.computeAttributes();
  this.execute();
};

/*
Compute the internal state of the widget
*/
DelayAction.prototype.execute = function() {
  this.delay = this.getAttribute("delay", 0)*1000;
  this.actions = this.getAttribute("actions", '');
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
DelayAction.prototype.refresh = function(changedTiddlers) {
  const changedAttributes = this.computeAttributes();
  if(Object.keys(changedAttributes).length > 0) {
    this.refreshSelf();
    return true;
  }
  return this.refreshChildren(changedTiddlers);
};

DelayAction.prototype.allowActionPropagation = function() {
	return false;
}

/*
Invoke the action associated with this widget
*/
DelayAction.prototype.invokeAction = function(triggeringWidget,event) {
	const self = this;
	this.makeChildWidgets();
  setTimeout(function() {
  	self.invokeActions(self,event);
		self.invokeActionString(self.actions,self,event);
  }, this.delay);
  return false; // Action was invoked
};

exports["action-delay"] = DelayAction;

})();
